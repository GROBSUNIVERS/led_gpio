# ISD_Aflevering_02 - Makefile
# Author - Peter Boye Hansen & Mathias Kruse

# Compiler definition
CC = arm-linux-gnueabihf-gcc

# Source files
SRCS = fileio.c gpio.c main.c

# Object files
OBJS = $(SRCS:%.c=build/%.o)

# Output executable
TARGET = ledgpio

# Build directory
BUILD_DIR = build/

# Binary directory
BINARY_DIR = binary/

# Install directory
INSTALL_DIR = /bin

# Create build and binary directories
$(shell mkdir -p $(BUILD_DIR) $(BINARY_DIR))

all: $(TARGET)

# Executable target
$(TARGET): $(OBJS)
	$(CC) -o $(BINARY_DIR)$@ $^ $(LIBS)

# Obj. file comp. rule
$(BUILD_DIR)%.o: src/%.c
	$(CC) -c $^ -o $@ 

# clean target
clean:
	rm -f $(BUILD_DIR)*.* $(BINARY_DIR)$(TARGET)

# install target
install: $(TARGET)
	cp $(BINARY_DIR)$(TARGET) $(INSTALL_DIR)

# uninstall target
uninstall:
	rm -f $(INSTALL_DIR)$(TARGET)
