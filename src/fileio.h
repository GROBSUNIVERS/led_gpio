/** fileio.h
 * GPIO BBB.
 * Author: Peter B. Hansen & Matthias Kruse Nielsen
 * Copyright: MIT.
 **/

#ifndef FILEIO_H
#define FILEIO_H

/** file_write
 * @brief writes byte array to file.
 * @param arr[] bytes to write.
 * @param path[] zero terminated string of filepath.
 * @return -1 on failure, 0 on success.
 **/
int file_write(char path[], char arr[]);
 
/** file_write
 * @brief  returns fd of opened file.
 * @param path[] path to file.
 **/
int get_fd(char path[]);

#endif
