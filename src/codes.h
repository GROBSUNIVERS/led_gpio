#ifndef CODES_H
#define CODES_H

#define ERROR -1
#define SUCCESS 0

#define ERROR_FOPEN      "ERROR: Opening file failed"
#define ERROR_GPIO_SETUP "ERROR: GPIO setup failed"
#define ERROR_GPIO_POLL  "ERROR: GPIO poll failed" 
#define ERROR_GPIO_VALUE "ERROR: Failed setting GPIO value"
#define ERROR_GPIO_INIT  "ERROR: Failed to initialize GPIO hardware"

#define INFO_GPIO_POLLING "INFO: Polling GPIO, CTRL C for exit..\n"
#define INFO_CLEAN_EXIT   "\nINFO: Control C detected, exiting.\n"

#endif


