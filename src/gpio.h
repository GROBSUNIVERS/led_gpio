/** gpio.h
 * GPIO BBB.
 * Author: Peter B. Hansen
 * Copyright: MIT.
 **/

#ifndef GPIO_H
#define GPIO_H

#define GPIO_EXPORT_PATH "/sys/class/gpio/export"
#define GPIO_UNEXPORT_PATH "/sys/class/gpio/unexport"
#define GPIO_PATH "/sys/class/gpio/gpio"
#define IN 0
#define OUT 1
#define NONE 0
#define RISING 1
#define FALLING 2
#define BOTH 3

/* Lets make some not-so-globals. */				 
static char dir[][4] = {"in", "out"};
static char edge[][8] = {"none", "rising", "falling", "both"};

/** @struct GPIO
 * @brief GPIO pin abstraction.
 * @var gpio is GPIO number.
 * @var direction integer direction of GPIO. Values: "in"/"out".
 * @var integer trigger for poll(). Values: "none", 
 * "rising", "falling" or "both".
 **/
typedef struct GPIO {
   int gpio;
   int direction;
   int edge;
} GPIO;

/** gpio_init
 * @brief sets up a GPIO. 
 * @param gpio struct.
 * @return -1 on failure, 0 on success.
 **/
int gpio_init(GPIO gpio);

/** gpio_destroy
 * @brief closes/unexports gpio.
 * @param gpio struct.
 * @return -1 on failure, 0 on success.
 **/
int gpio_destroy(GPIO gpio);

/** gpio_poll
 * @bries polls gpio.
 * @param gpio struct
 * @return -1 on failure else value of pin.
 **/
int gpio_poll(GPIO gpio);

/** gpio_set_value
 * Sets value on exported GPIO. 
 * @param gpio gpio structure.
 * @param val 0 or 1.
 **/
int gpio_set_value(GPIO gpio, int val);



#endif
