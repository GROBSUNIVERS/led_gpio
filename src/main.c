#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include "gpio.h"
#include "codes.h"

/* Volatile cz they can be changed by interrupt. */
volatile static GPIO button;
volatile static GPIO led;

/** cleanup
 * @brief signalhandler for keyboard interrupt AKA CTRL D.
 * Cleans up in sysfs.
 * */

static void cleanup(int sig){
   
   /* "Ignore" signal. */
   signal(sig, SIG_IGN); 

   /* Clean up. */
   gpio_destroy(button);
   gpio_destroy(led);

   printf(INFO_CLEAN_EXIT);
   
   /* Bye bye. */
   exit(0);
}

/** main
 * @brief execution starts here.
 **/

int main()
{
   int val = 1;
   
   /* Run cleanup on CTRL C. */
   signal(SIGINT, cleanup);
   
   /* Setting up buttons. */
   button.gpio = 67;
   button.direction = IN;
   button.edge = BOTH;
   
   /* And LED. */
   led.gpio = 66;
   led.direction = OUT;

   /* Init GPIO. */
   if(gpio_init(button) || gpio_init(led)){
      perror(ERROR_GPIO_INIT);
      return ERROR;
   }    

   printf(INFO_GPIO_POLLING);
   
   /* Run until interrupted. */
   while(1){
      val = gpio_poll(button);
      gpio_set_value(led, val);   
   }
   
   return SUCCESS;
}
