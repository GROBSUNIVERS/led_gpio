/** gpio.h
 * GPIO BBB.
 * Author: Peter B. Hansen & Matthias Kruse Nielsen.
 * Copyright: MIT.
 **/
 
#include <stdio.h>
#include <unistd.h> 
#include <fcntl.h>
#include "codes.h"

int get_fd(char path[]){
   int fd;
   char tmp;
   
   /* Using syscall to get fd. */
   fd = open(path, O_RDONLY);
   
   read(fd, &tmp, 1); // Empty readbuffer.
   fflush(stdout); // Better safe than sry..
   
   if(!fd){
      perror(ERROR_GPIO_POLL);
      return ERROR;
   }
   
   return fd;
}
   
	
int file_write(char path[], char val[]){
   
   /* Open file for writing the fopen way, as we dont care about fd. */

   FILE * file = fopen(path, "w");

   if(!file){
      perror(ERROR_FOPEN);
      return ERROR;
   }
   
   /* Write line to file. */
   fprintf(file, "%s", val);
   
   /* Clean up. */
   fclose(file);

   return SUCCESS;
}
