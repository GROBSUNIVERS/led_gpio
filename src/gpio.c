/** gpio.h
 * GPIO BBB.
 * Author: Peter B. Hansen
 * Copyright: MIT.
 **/
 
#include <stdio.h>
#include <string.h>
#include <sys/poll.h>
#include <unistd.h>
#include <fcntl.h>
#include "gpio.h"
#include "codes.h"
#include "fileio.h"

int gpio_init(GPIO gpio){

   char gpio_path[256];
   char gpio_number[3];

      
   /* Converting number to string equivalent */
   sprintf(gpio_number, "%d", gpio.gpio);

   /* Exporting the GPIO */
   for(int i = 1; i < 3;i++){
      gpio_destroy(gpio);
      if(file_write(GPIO_EXPORT_PATH, gpio_number) == ERROR){
         perror(ERROR_GPIO_SETUP);
         return ERROR;
      }
   }
   
   /* Building path for direction. */
   strcpy(gpio_path, GPIO_PATH);
   strcat(gpio_path, gpio_number);
   strcat(gpio_path, "/direction");

   /* Setting direction. */
   if(file_write(gpio_path, dir[gpio.direction]) == ERROR){
      perror(ERROR_GPIO_SETUP);
      return ERROR;
   }

   /* Setting edge only if input. */
   if(gpio.direction == IN){
     
      /* Building path... this is gettin' old. */
      strcpy(gpio_path, GPIO_PATH);
      strcat(gpio_path, gpio_number);
      strcat(gpio_path, "/edge");
            
      if(!file_write(gpio_path, edge[gpio.edge]) == ERROR){
	 perror(ERROR_GPIO_SETUP);
	 return ERROR;
      }
      
   }

   return SUCCESS;
}

int gpio_set_value(GPIO gpio, int val){
   char gpio_path[255];
   char gpio_num[3];
   char tmp[1];
   
   /* We feel so much better together. */
   sprintf(gpio_num, "%d", gpio.gpio);     
   strcpy(gpio_path, GPIO_PATH);
   strcat(gpio_path, gpio_num);   
   strcat(gpio_path, "/value");
   
   /* Make 0 term. string. */
   sprintf(tmp, "%d", val);

   /* Set value. */   
   if(file_write(gpio_path, tmp) == ERROR){
      perror(ERROR_GPIO_VALUE);
      return ERROR;
   }

}

int gpio_destroy(GPIO gpio){
   char gpio_num[3];
   
   /* convert GPIO number to string. */
   sprintf(gpio_num, "%d", gpio.gpio);
   
   /* Try to unexport the GPIO. */
   if(file_write(GPIO_UNEXPORT_PATH, gpio_num) == ERROR){
      perror(ERROR_GPIO_SETUP);
      return ERROR;
   }

   return SUCCESS;
}

int gpio_poll(GPIO gpio){
   char gpio_num[3];
   char buf;
   int val = ERROR;
   char gpio_path[256];

   // TODO move pollfd to GPIO struct and fill on init.
   struct pollfd p_fd;
   
   /* Dirty memory, now clean memory. */
   memset(&p_fd, 0, sizeof(struct pollfd));
   
   /* Cooooncatenations. */
   sprintf(gpio_num, "%d", gpio.gpio);     
   strcpy(gpio_path, GPIO_PATH);
   strcat(gpio_path, gpio_num);   
   strcat(gpio_path, "/value");
   
   /* Set up poll(2) to listen for events on fd).*/
   p_fd.fd = get_fd(gpio_path);
   p_fd.events = POLLPRI;
   
   /* Poll with timeout */
   poll(&p_fd, 1, -1);
   
   if(p_fd.revents & POLLPRI){
      
      /* re- re -wind, when the... */
      lseek(p_fd.fd, 0, SEEK_SET);
      read(p_fd.fd, &buf, 1);
      
      /* Convert to number */
      val = buf - '0';
      
   }
   
   close(p_fd.fd);    
   return val;
}
